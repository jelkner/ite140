import numpy as np


array1 = np.array([10, 100, 1000.])
array2 = np.array([[1., 2., 3.],
                   [4., 5., 6.]])

print(f'array2 + 42: {array2 + 42}')
print(f'array2 * array2: {array2 * array2}')
print(f'array2 @ array2.T: {array2 @ array2.T}')

import datetime as dt


timestamp = dt.datetime(2022, 6, 24, 14, 16)
print(f"timestamp: {timestamp}")
print(f"timestamp.day: {timestamp.day}")
some_time_ago = dt.datetime(2020, 6, 24, 12, 0)
print(f"some_time_ago: {some_time_ago}")
time_diff = timestamp - some_time_ago
print(f"timestamp - some_time_ago: {time_diff}")
today = dt.datetime.strptime("24.6.2022", "%d.%m.%Y")
print(f"today: {today}")

import pandas as pd


rainfall = pd.DataFrame([[300.1, 400.3, 1000.5],
                         [100.2, 300.4, 1100.6]],
                        columns=['City 1', 'City 2', 'City 3'])

print(rainfall)
print(f'Mean rainfall:\n{rainfall.mean()}')
print(f'Mean rainfall by row:\n{rainfall.mean(axis=1)}')

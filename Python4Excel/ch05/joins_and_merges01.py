import pandas as pd


df1 = pd.DataFrame(data=[[1, 2],
                         [3, 4],
                         [5, 6]],
                   columns=['A', 'B'])
df2 = pd.DataFrame(data=[[10, 20],
                         [30, 40]],
                   columns=['C', 'D'],
                   index=[1, 3])


print(f'df1:\n {df1}')
print(f'df2:\n {df2}')
print(f'Inner Join:\n{df1.join(df2, how="inner")}')
print(f'Left Join:\n{df1.join(df2, how="left")}')
print(f'Right Join:\n{df1.join(df2, how="right")}')
print(f'Outer Join:\n{df1.join(df2, how="outer")}')

df1['category'] = ['a', 'b', 'c']
df2['category'] = ['c', 'b']

print(f'df1:\n {df1}')
print(f'df2:\n {df2}')
print('Inner Merge on category:')
print(df1.merge(df2, how="inner", on=["category"]))
print('Left Merge on category:')
print(df1.merge(df2, how="left", on=["category"]))

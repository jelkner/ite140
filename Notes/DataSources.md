# Where to Find Good Data

Wikipedia defines [data science](https://en.wikipedia.org/wiki/Data_science)
as "an interdisciplinary field that uses scientific methods, processes,
algorithms and systems to extract or extrapolate knowledge and insights from
noisy, structured and unstructured data.

Since **data** is the object of our investigation, one of the challenges we
face at [ACC](https://careercenter.apsva.us/) in launching a proper data
science program is to find good sources of data.  "Good" in our context
should mean:

1. Useful as practical input to the methods, processes, algorithms, and
   systems we will learn to deploy in our study.
2. Intrinsically interesting and thus naturally engaging to our high school
   students who will be learning data science.


## Aggregate Site Links

* [50 Best Open Data Sources Ready to be Used Right Now](https://learn.g2.com/open-data-sources)
* [Davenport University Data Library Guide](https://davenport.libguides.com/data/public-data) 
* [Data Science for Social Good: Best Sources for Free Open Data](https://towardsdatascience.com/data-science-for-social-good-best-sources-for-free-open-data-5120070caf02)


## Digging Deeper: Looking at Particular Sources

* [DATA.GOV](https://data.gov/)
* [US Census Bureau Data](https://www.census.gov/data.html)


## In Our Neighborhood: Local Data Sources

* [Arlington Open Data](https://data.arlingtonva.us/)

# Processing Data

If the purpose of [data science](https://en.wikipedia.org/wiki/Data_science)
is to *extract or extrapolate knowledge and insights* from data, two paths
from opposite ends of the journey suggest themselves:

1. **Data first** - We start as investigators with the data and conduct an
   open ended search for patterns within in that may reveal knowledge or
   insight.
2. **[It's the Question that Drives Us](http://ehumandawn.blogspot.com/2013/12/its-question-that-drives-us.html)** -
   We start with a question already framed and look for data to help us answer
   the question.

In practice, both processes are probably always at play at the same time.
A few years I began with the question of who in Arlington supported the
street car along Columbia Pike, and produced
[http://www.openbookproject.net/tutorials/explore_fsgis/](http://www.openbookproject.net/tutorials/explore_fsgis/)
as a result.

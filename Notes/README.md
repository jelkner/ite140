# Notes

Documents related to our investigation of data science.

## Document List

* [Data sources](DataSources.md)
* [Data cleaning](DataCleaning.md)
* [Processing data](ProcessingData.md)

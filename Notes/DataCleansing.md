# Data Cleaning

According to [Wikipedia](https://en.wikipedia.org/wiki/Data_cleansing),
**data cleaning** is "the process of detecting and correcting (or removing)
corrupt or inaccurate records from a record set, table, or database".

## What A Curious Moon Has to Say About Data Cleaning

Our text for the Spring course,
[A Curios Moon](https://bigmachine.io/products/a-curious-moon/) has this to
say about data cleaning, which it calls **Transformation** as part of the
[ELT](https://www.ibm.com/cloud/learn/elt) process beginning at the bottom of
page 31:

> Give me a CSV, and I’ll show you a data problem, somewhere. Human beings are
> naturally not mentally equipped to be custodians of data. They’re messy by
> nature, working off of vaguery and intuition in the face of pure logic. The
> spreadsheet is a curious crossroad between the human and machine mind.
>
> In this setting, *we work for the machines*. If they are to do our bidding
> they need correct information: it comprises a unique set of failures for both
> of us.
>
> When the data is extracted, it’s usually sent along in CSV format to 
> whatever system is going to load it.  CSVs are garbage. If we import garbage
> into our sanitary, pristine database it becomes a rotten, filthy mess. We
> don’t want that, so we need to ensure three things:
>
> * **Correct typing**. Some people think that -- is somehow a meaningful
>   substitute for a number. Other times you’ll see the ever-helpful “N/A.”
>   These must be corrected, carefully.
> * **Completeness**.  Rows and rows of data will probably need to be thrown
>   out because they are incomplete, or because we don’t know how to deal with
>   their typing.
> * **Accuracy**. One of the columns in the particle data that you will be
>   loading is ``SPACESHIP_RIGHT_ASCENSION``, another is
>   ``SPACECRAFT_DECLINATION``. These  values have units in angular degrees,
>   angular degrees have an upper bound (90, 180, 360, e.g., depending on the
>   system); therefore, data including a declination of 720 degrees would
>   clearly be inadmissible.  Data with a declination of 110 degrees *might*
>   be.
>
> This is the most involved part of the ETL process, and the most
> failure-prone. The last thing we want to do is to make data *incorrect*
> while trying to correct it. This could result in Seriously Bad Situations if
> people and teams are basing their decisions on the data we provide. Our goal
> is correct, complete, accurate data.

The article
[Guide To Data Cleaning: Definition, Benefits, Components, And How To Clean
Your Data](https://www.tableau.com/learn/articles/what-is-data-cleaning)
provides a more detailed introduction to this topic.

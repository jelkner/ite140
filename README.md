# ITE 140: Spreadsheet Software Source Files

Python programs for use with NVCC ITE 140: Spreadsheet Software, using a data
science approach to the course.

## Authors and acknowledgment

Contributors include:

* Jeffrey Elkner
* Aaron Rabach

*NOTE*: The repo for the follow up course to this [ITD 256: Advanced Database
Management](https://courses.vccs.edu/courses/ITD256-Advanced%20Database%20Management)
is [here](https://gitlab.com/jelkner/itd256).

## License

GPLv3

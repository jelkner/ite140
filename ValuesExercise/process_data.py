import pandas as pd


def load_data():
    """
      Load an excel spreadsheet, AnonymizedResponsesToValuesExercise.xlsx,
      into a DataFrame, remove the timestamp column, and set usable
      column headings.
    """
    # Read raw data from xlsx file
    data = pd.read_excel('AnonymizedResponsesToValuesExercise.xlsx')
    # Remove time stamp column and empty columns at end
    data = data.iloc[:, 1:5]
    # Set column heading to something sensible
    data.columns = ['Top10', 'Top5', 'Top3', 'Top']
    # Remove empty rows
    data = data.dropna(how="all")
    return data


def create_choices_table(data):
    """
     Create a DataFrame with column for each of the choices in choices.txt
     and a row for respondent in the reponse DataFrame, data, passed in.
    """
    # Read choices from file
    num_responses = len(data) 
    f = open('choices.txt', 'r')
    choices = [choice.strip() for choice in f.readlines()]
    data = {}
    for choice in choices:
        data[choice] = [False] * num_responses
    return pd.DataFrame(data=data)


if __name__ == '__main__':
    data = load_data()
    top10table = create_choices_table(data)

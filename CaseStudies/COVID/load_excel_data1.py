"""
Test program to load data from an excel spreadsheet using pandas.
"""
import pandas as pd

data = pd.read_excel("resources/covid-key-countries-pivoted.xlsx")
print(data)
